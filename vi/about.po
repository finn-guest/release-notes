# Vietnamese translation for Release Notes (About).
# Copyright © 2009 Free Software Foundation, Inc.
# Clytie Siddall <clytie@riverland.net.au>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"POT-Creation-Date: 2017-01-26 23:48+0100\n"
"PO-Revision-Date: 2009-02-03 17:42+1030\n"
"Last-Translator: Clytie Siddall <clytie@riverland.net.au>\n"
"Language-Team: Vietnamese <vi-VN@googlegroups.com>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LocFactoryEditor 1.8\n"

#. type: Attribute 'lang' of: <chapter>
#: en/about.dbk:8
msgid "en"
msgstr "vi"

#. type: Content of: <chapter><title>
#: en/about.dbk:9
msgid "Introduction"
msgstr "Giới thiệu"

#. type: Content of: <chapter><para>
#: en/about.dbk:11
#, fuzzy
#| msgid ""
#| "This document informs users of the &debian; distribution about major "
#| "changes in version &release; (codenamed \"&releasename;\")."
msgid ""
"This document informs users of the &debian; distribution about major changes "
"in version &release; (codenamed &releasename;)."
msgstr ""
"Tài liệu này báo người dùng bản phát hành &debian; biết về những thay đổi "
"chính trong phiên bản &release; (tên mã « &releasename; »)."

#. type: Content of: <chapter><para>
#: en/about.dbk:15
msgid ""
"The release notes provide information on how to upgrade safely from release "
"&oldrelease; (codenamed &oldreleasename;) to the current release and inform "
"users of known potential issues they could encounter in that process."
msgstr ""
"Ghi chú Phát hành thì cung cấp thông tin về cách nâng cấp an toàn từ bản "
"phát hành &oldrelease; (tên mã « &oldreleasename; ») lên bản phát hành hiện "
"thời, và cho người dùng biết về các vấn đề được biết có thể gặp trong quá "
"trình đó."

#. type: Content of: <chapter><para>
#: en/about.dbk:21
msgid ""
"You can get the most recent version of this document from <ulink url=\"&url-"
"release-notes;\"></ulink>.  If in doubt, check the date on the first page to "
"make sure you are reading a current version."
msgstr ""
"Người dùng có thể lấy phiên bản mới nhất của tài liệu này ở <ulink url="
"\"&url-release-notes;\"></ulink>.  Chưa chắc thì kiểm tra ngày tháng trên "
"trang đầu, để tìm biết nếu tài liệu đó là một phiên bản hiện thời không."

#. type: Content of: <chapter><caution><para>
#: en/about.dbk:27
msgid ""
"Note that it is impossible to list every known issue and that therefore a "
"selection has been made based on a combination of the expected prevalence "
"and impact of issues."
msgstr ""
"Ghi chú rằng không thể liệt kê được mọi vấn đề đã biết, vì thế chúng tôi cố "
"gắng chọn những vấn đề thường gặp nhất và có tác động lớn nhất."

#. type: Content of: <chapter><para>
#: en/about.dbk:33
#, fuzzy
#| msgid ""
#| "Please note that we only support and document upgrading from the previous "
#| "release of Debian (in this case, the upgrade from &oldrelease;).  If you "
#| "need to upgrade from older releases, we suggest you read previous "
#| "editions of the release notes and upgrade to &oldrelease; first."
msgid ""
"Please note that we only support and document upgrading from the previous "
"release of Debian (in this case, the upgrade from &oldreleasename;).  If you "
"need to upgrade from older releases, we suggest you read previous editions "
"of the release notes and upgrade to &oldreleasename; first."
msgstr ""
"Ghi chú rằng chúng tôi chỉ hỗ trợ và mô tả trong tài liệu quá trình nâng cấp "
"từ bản phát hành Debian trước (trong trường hợp này, nâng cấp từ "
"&oldrelease;). Nếu người dùng cần phải nâng cấp từ một phiên bản cũ hơn, hãy "
"đọc phiên bản Ghi chú Phát hành tương ứng và nâng cấp lên &oldrelease; trước "
"tiên."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:41
msgid "Reporting bugs on this document"
msgstr "Thông báo lỗi về tài liệu này"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:43
msgid ""
"We have attempted to test all the different upgrade steps described in this "
"document and to anticipate all the possible issues our users might encounter."
msgstr ""
"Chúng tôi đã cố gắng thử tất cả các bước nâng cấp khác nhau được diễn tả "
"trong tài liệu, và để đoán trước tất cả các vấn đề mà người dùng có thể gặp."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:48
#, fuzzy
#| msgid ""
#| "Nevertheless, if you think you have found a bug (incorrect information or "
#| "information that is missing)  in this documentation, please file a bug in "
#| "the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
#| "<systemitem role=\"package\">release-notes</systemitem> package."
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or "
"information that is missing)  in this documentation, please file a bug in "
"the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
"<systemitem role=\"package\">release-notes</systemitem> package. You might "
"want to review first the <ulink url=\"&url-bts-rn;\">existing bug reports</"
"ulink> in case the issue you've found has already been reported. Feel free "
"to add additional information to existing bug reports if you can contribute "
"content for this document."
msgstr ""
"Tuy nhiên, nếu bạn thấy vẫn có một lỗi (thông tin không đúng hay còn thiếu) "
"trong tài liệu này, hãy gửi một báo cáo lỗi dùng <ulink url=\"&url-bts;\">hệ "
"thống theo dõi lỗi</ulink> đối với gói tên <systemitem role=\"package"
"\">release-notes</systemitem>."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:60
msgid ""
"We appreciate, and encourage, reports providing patches to the document's "
"sources. You will find more information describing how to obtain the sources "
"of this document in <xref linkend=\"sources\"/>."
msgstr ""

#. type: Content of: <chapter><section><title>
#: en/about.dbk:68
msgid "Contributing upgrade reports"
msgstr "Đóng góp báo cáo nâng cấp"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:70
msgid ""
"We welcome any information from users related to upgrades from "
"&oldreleasename; to &releasename;.  If you are willing to share information "
"please file a bug in the <ulink url=\"&url-bts;\">bug tracking system</"
"ulink> against the <systemitem role=\"package\">upgrade-reports</systemitem> "
"package with your results.  We request that you compress any attachments "
"that are included (using <command>gzip</command>)."
msgstr ""
"Chúng tôi hoan nghênh các bạn đóng góp thông tin về tiến trình nâng cấp từ "
"&oldreleasename; lên &releasename;. Nếu bạn muốn chia sẻ thông tin, hãy gửi "
"một báo cáo lỗi dùng <ulink url=\"&url-bts;\">hệ thống theo dõi lỗi</ulink> "
"đối với gói tên <systemitem role=\"package\">upgrade-reports</systemitem>, "
"bao gồm kết quả của tiến trình. Cũng có thể đính kèm tập tin, miễn là bạn "
"nén tập tin (dùng <command>gzip</command>) trước khi đính kèm báo cáo."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:79
msgid ""
"Please include the following information when submitting your upgrade report:"
msgstr "Hãy bao gồm thông tin theo đây khi đệ trình báo cáo nâng cấp:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:86
#, fuzzy
#| msgid ""
#| "The status of your package database before and after the upgrade: "
#| "<command>dpkg</command>'s status database available at <filename>/var/lib/"
#| "dpkg/status</filename> and <command>aptitude</command>'s package state "
#| "information, available at <filename>/var/lib/aptitude/pkgstates</"
#| "filename>.  You should have made a backup before the upgrade as described "
#| "at <xref linkend=\"data-backup\"/>, but you can also find backups of this "
#| "information in <filename>/var/backups</filename>."
msgid ""
"The status of your package database before and after the upgrade: "
"<systemitem role=\"package\">dpkg</systemitem>'s status database available "
"at <filename>/var/lib/dpkg/status</filename> and <systemitem role=\"package"
"\">apt</systemitem>'s package state information, available at <filename>/var/"
"lib/apt/extended_states</filename>.  You should have made a backup before "
"the upgrade as described at <xref linkend=\"data-backup\"/>, but you can "
"also find backups of <filename>/var/lib/dpkg/status</filename> in <filename>/"
"var/backups</filename>."
msgstr ""
"Trạng thái của cơ sở dữ liệu gói trước và sau khi nâng cấp: trên đĩa của "
"người dùng, cơ sở dữ liệu trạng thái của chương trình <command>dpkg</"
"command> nằm trong thư mục <filename>/var/lib/dpkg/status</filename>, và "
"thông tin tình trạng gói của <command>aptitude</command> nằm ở <filename>/"
"var/lib/aptitude/pkgstates</filename>. Trước khi nâng cấp, người dùng nên "
"sao lưu dữ liệu như được diễn tả ở <xref linkend=\"data-backup\"/> , nhưng "
"cũng có bản sao lưu của thông tin này trong thư mục <filename>/var/backups</"
"filename>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:99
msgid ""
"Session logs created using <command>script</command>, as described in <xref "
"linkend=\"record-session\"/>."
msgstr ""
"Bản ghi phiên chạy được tạo dùng <command>script</command>, như diễn tả "
"trong <xref linkend=\"record-session\"/> ."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:105
#, fuzzy
#| msgid ""
#| "Your <systemitem role=\"package\">apt</systemitem> logs, available at "
#| "<filename>/var/log/apt/term.log</filename> or your <command>aptitude</"
#| "command> logs, available at <filename>/var/log/aptitude</filename>."
msgid ""
"Your <systemitem role=\"package\">apt</systemitem> logs, available at "
"<filename>/var/log/apt/term.log</filename>, or your <command>aptitude</"
"command> logs, available at <filename>/var/log/aptitude</filename>."
msgstr ""
"Các bản ghi <systemitem role=\"package\">apt</systemitem> ở<filename>/var/"
"log/apt/term.log</filename>, hoặc các bản ghi <command>aptitude</command> ở "
"<filename>/var/log/aptitude</filename>."

#. type: Content of: <chapter><section><note><para>
#: en/about.dbk:114
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug report "
"as the information will be published in a public database."
msgstr ""
"Người dùng cũng nên bỏ chút thời gian xem lại và gỡ bỏ khỏi bản ghi bất kỳ "
"thông tin vẫn nhạy cảm hay mật, trước khi bao gồm bản ghi trong một báo cáo "
"lỗi, vì thông tin được đệ trình sẽ được xuất bản trong một cơ sở dữ liệu "
"công cộng."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:123
msgid "Sources for this document"
msgstr "Nguồn của tài liệu này"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:125
msgid ""
"The source of this document is in DocBook XML<indexterm><primary>DocBook "
"XML</primary></indexterm> format. The HTML version is generated using "
"<systemitem role=\"package\">docbook-xsl</systemitem> and <systemitem role="
"\"package\">xsltproc</systemitem>. The PDF version is generated using "
"<systemitem role=\"package\">dblatex</systemitem> or <systemitem role="
"\"package\">xmlroff</systemitem>. Sources for the Release Notes are "
"available in the SVN repository of the <emphasis>Debian Documentation "
"Project</emphasis>.  You can use the <ulink url=\"&url-vcs-release-notes;"
"\">web interface</ulink> to access its files individually through the web "
"and see their changes.  For more information on how to access the SVN please "
"consult the <ulink url=\"&url-ddp-vcs-info;\">Debian Documentation Project "
"SVN information pages</ulink>."
msgstr ""
"Mã nguồn của tài liệu này theo định dạng DocBook "
"XML<indexterm><primary>DocBook XML</primary></indexterm>. Phiên bản HTML "
"được tạo ra dùng <systemitem role=\"package\">docbook-xsl</systemitem> và "
"<systemitem role=\"package\">xsltproc</systemitem>. Phiên bản PDF được tạo "
"ra dùng <systemitem role=\"package\">dblatex</systemitem> hay <systemitem "
"role=\"package\">xmlroff</systemitem>. Nguồn của Ghi chú Phát hành cũng sẵn "
"sàng trong kho SVN của <emphasis>Dự án Tài liệu Debian</emphasis>.  Người "
"dùng có thể sử dụng <ulink url=\"&url-vcs-release-notes;\">giao diện Web</"
"ulink> để truy cập đến mỗi tập tin riêng thông qua Web và thấy các thay đổi. "
"Để tìm thêm thông tin về cách truy cập đến kho SVN, xem <ulink url=\"&url-"
"ddp-svn-info;\">các trang Thông tin SVN của Dự án Tài liệu Debian</ulink>."

#~ msgid ""
#~ "\n"
#~ "TODO: check status of #494028 about apt-get vs. aptitude\n"
#~ "TODO: any more things to add here?\n"
#~ msgstr ""
#~ "\n"
#~ "CẦN LÀM: kiểm tra trạng thái của lỗi #494028 về apt-get so với aptitude\n"
#~ "CẦN LÀM: có gì khác cần thêm vào đây ?\n"

#~ msgid ""
#~ "As listed on the front page of the PDF version and in the footer of the "
#~ "HTML version."
#~ msgstr ""
#~ "Như được ghi trên trang chính của phiên bản PDF và trong phần đầu trang "
#~ "của phiên bản HTML."

#~ msgid "</footnote>, you might wish to obtain the latest version."
#~ msgstr "</footnote>, khuyên bạn lấy phiên bản mới nhất."
