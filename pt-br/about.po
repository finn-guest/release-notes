# Translation of about.po to Brazilian Portuguese language.
# Translation of Debian release notes to Brazilian Portuguese.
# Copyright (C) 2009-2017 Debian Brazilian Portuguese l10n team
# <debian-l10n-portuguese@lists.debian.org>
# This file is distributed under the same license as the Debian release notes.
#
# Translator: Felipe Augusto van de Wiel <faw@debian.org>, -2009.
#             Adriano Rafael Gomes <adrianorg@debian.org>, 2019.
# Revisors: Éverton M. Arruda Jr. (Notrev) <notrev@gmail.com>, 2011.
#           Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2011-2017.
#           Adriano Rafael Gomes <adrianorg@arg.eti.br>, 2013-2015.
#
msgid ""
msgstr ""
"Project-Id-Version: Release Notes\n"
"POT-Creation-Date: 2019-03-17 19:05-0300\n"
"PO-Revision-Date: 2019-03-16 18:40-0300\n"
"Last-Translator: Adriano Rafael Gomes <adrianorg@debian.org>\n"
"Language-Team: l10n Brazilian Portuguese <debian-l10n-portuguese@lists."
"debian.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute 'lang' of: <chapter>
#: en/about.dbk:8
msgid "en"
msgstr "pt_BR"

#. type: Content of: <chapter><title>
#: en/about.dbk:9
msgid "Introduction"
msgstr "Introdução"

#. type: Content of: <chapter><para>
#: en/about.dbk:11
msgid ""
"This document informs users of the &debian; distribution about major changes "
"in version &release; (codenamed &releasename;)."
msgstr ""
"Este documento dá aos usuários da distribuição &debian; informações sobre "
"grandes mudanças na versão &release; (codinome &releasename;)."

#. type: Content of: <chapter><para>
#: en/about.dbk:15
msgid ""
"The release notes provide information on how to upgrade safely from release "
"&oldrelease; (codenamed &oldreleasename;) to the current release and inform "
"users of known potential issues they could encounter in that process."
msgstr ""
"As notas de lançamento fornecem informações sobre como atualizar de forma "
"segura a partir da versão &oldrelease; (codinome &oldreleasename;) para a "
"versão atual e dá aos usuários informações sobre potenciais problemas "
"conhecidos que eles possam encontrar nesse processo."

#. type: Content of: <chapter><para>
#: en/about.dbk:21
msgid ""
"You can get the most recent version of this document from <ulink url=\"&url-"
"release-notes;\"></ulink>.  If in doubt, check the date on the first page to "
"make sure you are reading a current version."
msgstr ""
"Você pode obter a versão mais recente deste documento na <ulink url=\"&url-"
"release-notes;\"></ulink>. Em caso de dúvida, verifique a data na primeira "
"página do documento para ter certeza de que você está lendo uma versão "
"atualizada."

#. type: Content of: <chapter><caution><para>
#: en/about.dbk:27
msgid ""
"Note that it is impossible to list every known issue and that therefore a "
"selection has been made based on a combination of the expected prevalence "
"and impact of issues."
msgstr ""
"Note que é impossível listar todos os problemas conhecidos e portanto uma "
"seleção foi feita baseada numa combinação da quantidade esperada e do "
"impacto desses problemas."

#. type: Content of: <chapter><para>
#: en/about.dbk:33
msgid ""
"Please note that we only support and document upgrading from the previous "
"release of Debian (in this case, the upgrade from &oldreleasename;).  If you "
"need to upgrade from older releases, we suggest you read previous editions "
"of the release notes and upgrade to &oldreleasename; first."
msgstr ""
"Por favor, note que só damos suporte e documentamos a atualização a partir "
"da versão anterior do Debian (nesse caso, a atualização a partir da versão "
"&oldreleasename;). Caso você precise atualizar a partir de versões mais "
"antigas, nós sugerimos que você leia as edições anteriores das notas de "
"lançamento e atualize para a &oldreleasename; primeiro."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:41
msgid "Reporting bugs on this document"
msgstr "Reportando bugs neste documento"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:43
msgid ""
"We have attempted to test all the different upgrade steps described in this "
"document and to anticipate all the possible issues our users might encounter."
msgstr ""
"Nós tentamos testar todos os diferentes passos de atualizações descritos "
"neste documento bem como antecipar todos os possíveis problemas que nossos "
"usuários possam encontrar."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:48
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or "
"information that is missing)  in this documentation, please file a bug in "
"the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
"<systemitem role=\"package\">release-notes</systemitem> package. You might "
"first want to review the <ulink url=\"&url-bts-rn;\">existing bug reports</"
"ulink> in case the issue you've found has already been reported. Feel free "
"to add additional information to existing bug reports if you can contribute "
"content for this document."
msgstr ""
"Apesar disso, caso você acredite ter encontrado um bug (informação incorreta "
"ou informação que está faltando) nesta documentação, por favor, registre um "
"bug no <ulink url=\"&url-bts;\">sistema de rastreamento de bugs</ulink> para "
"o pacote <systemitem role=\"package\">release-notes</systemitem>. É "
"aconselhável que você reveja primeiro os <ulink url=\"&url-bts-rn;\"> "
"relatórios de bugs existentes</ulink> caso a questão que você encontrou já "
"tenha sido relatada. Sinta-se livre para acrescentar informações adicionais "
"aos relatórios de bugs existentes, caso você possa contribuir com conteúdo "
"para este documento."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:60
msgid ""
"We appreciate, and encourage, reports providing patches to the document's "
"sources. You will find more information describing how to obtain the sources "
"of this document in <xref linkend=\"sources\"/>."
msgstr ""
"Apreciamos, e encorajamos, relatórios fornecendo patches para o código fonte "
"deste documento. Você encontrará mais informações sobre como obter o código "
"fonte deste documento na <xref linkend=\"sources\"/>."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:68
msgid "Contributing upgrade reports"
msgstr "Contribuindo com relatórios de atualização"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:70
msgid ""
"We welcome any information from users related to upgrades from "
"&oldreleasename; to &releasename;.  If you are willing to share information "
"please file a bug in the <ulink url=\"&url-bts;\">bug tracking system</"
"ulink> against the <systemitem role=\"package\">upgrade-reports</systemitem> "
"package with your results.  We request that you compress any attachments "
"that are included (using <command>gzip</command>)."
msgstr ""
"Nós apreciamos quaisquer informações dos usuários relacionadas a "
"atualizações da &oldreleasename; para a &releasename;. Caso você esteja "
"interessado em compartilhar informação, por favor, registre um bug no <ulink "
"url=\"&url-bts;\">sistema de rastreamento de bugs</ulink> para o pacote "
"<systemitem role=\"package\">upgrade-reports</systemitem> com os seus "
"resultados. Nós pedimos que você compacte quaisquer anexos que venha a "
"incluir (usando o <command>gzip</command>)."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:79
msgid ""
"Please include the following information when submitting your upgrade report:"
msgstr ""
"Por favor, inclua as seguintes informações quando enviar seu relatório de "
"atualização:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:86
msgid ""
"The status of your package database before and after the upgrade: "
"<systemitem role=\"package\">dpkg</systemitem>'s status database available "
"at <filename>/var/lib/dpkg/status</filename> and <systemitem role=\"package"
"\">apt</systemitem>'s package state information, available at <filename>/var/"
"lib/apt/extended_states</filename>.  You should have made a backup before "
"the upgrade as described at <xref linkend=\"data-backup\"/>, but you can "
"also find backups of <filename>/var/lib/dpkg/status</filename> in <filename>/"
"var/backups</filename>."
msgstr ""
"O estado da sua base de dados de pacotes antes e depois da atualização: a "
"base de dados de estados do <systemitem role=\"package\">dpkg</systemitem> "
"está disponível em <filename>/var/lib/dpkg/status</filename> e a informação "
"do estado dos pacotes do <systemitem role=\"package\">apt</systemitem> está "
"disponível em <filename>/var/lib/apt/extended_states</filename>. Você deve "
"ter feito backup antes da atualização conforme descrito na <xref linkend="
"\"data-backup\"/>, mas você também pode encontrar backups do <filename>/var/"
"lib/dpkg/status</filename> em <filename>/var/backups</filename>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:99
msgid ""
"Session logs created using <command>script</command>, as described in <xref "
"linkend=\"record-session\"/>."
msgstr ""
"Registros da sessão criados usando o comando <command>script</command>, "
"conforme descrito na <xref linkend=\"record-session\"/>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:105
msgid ""
"Your <systemitem role=\"package\">apt</systemitem> logs, available at "
"<filename>/var/log/apt/term.log</filename>, or your <command>aptitude</"
"command> logs, available at <filename>/var/log/aptitude</filename>."
msgstr ""
"Seus logs do <systemitem role=\"package\">apt</systemitem>, disponíveis em "
"<filename>/var/log/apt/term.log</filename>, ou seus logs do "
"<command>aptitude</command>, disponíveis em <filename>/var/log/aptitude</"
"filename>."

#. type: Content of: <chapter><section><note><para>
#: en/about.dbk:114
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug report "
"as the information will be published in a public database."
msgstr ""
"Você deve usar algum tempo para revisar e remover qualquer informação "
"sensível e/ou confidencial dos logs antes de incluí-los no relatório de bug, "
"pois a informação será disponibilizada em um banco de dados público."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:123
msgid "Sources for this document"
msgstr "Código fonte deste documento"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:125
msgid ""
"The source of this document is in DocBook XML<indexterm><primary>DocBook "
"XML</primary></indexterm> format. The HTML version is generated using "
"<systemitem role=\"package\">docbook-xsl</systemitem> and <systemitem role="
"\"package\">xsltproc</systemitem>. The PDF version is generated using "
"<systemitem role=\"package\">dblatex</systemitem> or <systemitem role="
"\"package\">xmlroff</systemitem>. Sources for the Release Notes are "
"available in the Git repository of the <emphasis>Debian Documentation "
"Project</emphasis>.  You can use the <ulink url=\"&url-vcs-release-notes;"
"\">web interface</ulink> to access its files individually through the web "
"and see their changes.  For more information on how to access Git please "
"consult the <ulink url=\"&url-ddp-vcs-info;\">Debian Documentation Project "
"VCS information pages</ulink>."
msgstr ""
"O código fonte deste documento está no formato DocBook "
"XML<indexterm><primary>DocBook XML</primary></indexterm>. A versão HTML é "
"gerada usando <systemitem role=\"package\">docbook-xsl</systemitem> e "
"<systemitem role=\"package\">xsltproc</systemitem>. A versão PDF é gerada "
"usando <systemitem role=\"package\">dblatex</systemitem> ou <systemitem role="
"\"package\">xmlroff</systemitem>. Os códigos fonte das notas de lançamento "
"estão disponíveis no repositório Git do <emphasis>Projeto de Documentação "
"Debian</emphasis>. Você pode usar a <ulink url=\"&url-vcs-release-notes;"
"\">interface web</ulink> para acessar seus arquivos individualmente através "
"da web e ver suas mudanças. Para mais informações sobre como acessar o Git, "
"por favor, consulte as <ulink url=\"&url-ddp-vcs-info;\">páginas de "
"informação sobre VCS do Projeto de Documentação Debian</ulink>."

#~ msgid "TODO: any more things to add here?\n"
#~ msgstr "TODO: any more things to add here?\n"
